package Chromicle.RecycleBarrel;

/*
 * @author Chromicle
 * */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.read_btn)
    Button read;

    @BindView(R.id.read_all_btn)
    Button readAll;

    @BindView(R.id.update_btn)
    Button update;

    @BindView(R.id.delete_btn)
    Button delete;

    @BindView(R.id.insert_btn)
    Button insert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        read.startAnimation(shake);
        readAll.startAnimation(shake);
        update.startAnimation(shake);
        delete.startAnimation(shake);
        insert.startAnimation(shake);


    }


    @OnClick(R.id.read_all_btn)
    public void readAll() {


        if (InternetConnection.checkConnection(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), ReadAllData.class);
            startActivity(intent);

        } else {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }


    }


    @OnClick(R.id.insert_btn)
    public void insert() {


        if (InternetConnection.checkConnection(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), InsertData.class);
            startActivity(intent);


        } else {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }


    }


    @OnClick(R.id.update_btn)
    public void update() {


        if (InternetConnection.checkConnection(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), UpdateData.class);
            startActivity(intent);


        } else {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }


    }

    @OnClick(R.id.read_btn)
    public void read() {


        if (InternetConnection.checkConnection(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), ReadSingleData.class);
            startActivity(intent);


        } else {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }


    }

    @OnClick(R.id.delete_btn)
    public void delete() {


        if (InternetConnection.checkConnection(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), DeleteData.class);
            startActivity(intent);


        } else {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }


    }

}